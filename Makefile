TARGETDIR ?= ~/stowdir

.PHONY: all

all:
	stow -t $(TARGETDIR) -R src
